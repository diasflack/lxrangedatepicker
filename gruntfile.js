module.exports = function(grunt) {

    grunt.initConfig({
        copy: {
            main: {
                expand: true,
                flatten: true,
                src: ['src/js/*.js'],
                dest: 'dist/'
            }
        },
        jasmine : {
            // Your project's source files
            src : ["https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js", 'src/js/*.js'],
            options : {
                specs : 'spec/**/*[sS]pec.js',
                helpers : 'spec/helpers/*.js'
            }
        },
        jshint: {
            all: ['Gruntfile.js', 'src/js/**/*.js', 'spec/**/*.js']
        },
        uglify: {
            my_target: {
                files: {
                    'dist/lxrangedatepicker.min.js': ['src/js/*.js']
                }
            }
        },
        watch: {
            tests: {
                files: ['src/js/**/*.js', 'spec/**/*[sS]pec.js'],
                tasks: ['jasmine']
            }
        },
        jsdoc : {
            dist : {
                src: ['src/js/**/*.js'],
                options: {
                    destination: 'doc',
                    template : "node_modules/grunt-jsdoc/node_modules/ink-docstrap/template",
                    configure : "node_modules/grunt-jsdoc/node_modules/ink-docstrap/template/jsdoc.conf.json"
                }
            }
        }
    });

    // Register tasks.
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-jsdoc');

    // Default task.
    grunt.registerTask('default', 'jasmine');
    grunt.registerTask('build', ['copy','uglify']);
};