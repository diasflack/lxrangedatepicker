(function($){
    "use strict";

    var LXRangeDatepicker;

    $.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;
    $.datepicker._updateDatepicker = function(inst) {
        $.datepicker._updateDatepicker_original(inst);
        var afterShow = this._get(inst, 'afterShow');
        if (afterShow)
            afterShow.apply((inst.input ? inst.input[0] : null));
    };

    var isEqual = function (a, b) {
        return a[0] === b[0];
    };


    LXRangeDatepicker = {
        options: {
            $el: null,
            $label: null,
            $label_text: null,
            $inner_label: null,
            $dialog: null,
            $lxrangestart: null,
            $lxrangeend:null,
            dialog_parent_class:null,
            currDate: 0,
            start: -1,
            end: -1,
            changed: false,
            resetTimezone: true,
            ranges: [
                {
                    name:"Квартал",
                    range: function(currDate) {
                        var date, year, month, day;

                        date = new Date(currDate);

                        year = date.getFullYear();
                        month = date.getMonth()-3;
                        day = date.getDate();

                        if ([3,5,8,10,-2].indexOf(month) > -1 && day === 31) {
                            day = 30;
                        } else if ((month === 1) && [29,30,31].indexOf(day) > -1) {
                            day = 28;
                        }

                        return new Date(year, month, day).getTime();
                    }
                },
                {
                    name:"С начала месяца",
                    range: function(currDate) {
                        var date, year, month;

                        date = new Date(currDate);

                        year = date.getFullYear();
                        month = date.getMonth();

                        return new Date(year, month).getTime();
                    }
                },
                {
                    name:"С начала года",
                    range: function(currDate) {
                        var date, year;

                        date = new Date(currDate);
                        year = date.getFullYear();

                        return new Date(year, 0).getTime();
                    }
                }
            ]

        },

        _init: function() {
            this.options.$el = this.element;

            this.options.$label = this.options.$el.parents("label");
            this.options.$label_text = this.options.$label.find("span");
            this.options.$label_text.text("Свой период");

            this.options.currDate = new Date().getTime();

            this.options.$dialog = $(this._generateHTML());

            if (this.options.dialog_parent_class !== null) {
                this.options.$label.parents(this.options.dialog_parent_class).append(this.options.$dialog);
            } else {
                this.options.$label.append(this.options.$dialog);
            }

            this.options.$inner_label = this.options.$dialog.find(".lxrange-selected span");
            this.options.$inner_label.text("Выберите нужные даты начала и конца необходимого периода");

            this.options.$lxrangestart = this.options.$dialog.find(".lxrange-start");
            this.options.$lxrangeend = this.options.$dialog.find(".lxrange-end");

            this._initDatepickers();
            this._bindEvents();
        },

        _generateHTML: function() {
            var html, rangesLength = this.options.ranges.length;

            html = "<div class='lxrange-datepicker'>";

            html += "<p class='lxrange-selected'>Выбрано: <span></span></p>";

            html += "<div class='lxrange-pickers group'>";
            html += "<div class='lxrange-start group'></div>";
            html += "<div class='lxrange-middle'></div>";
            html += "<div class='lxrange-end group'></div>";
            html += "</div>";

            html += "<div class='lxrange-standard'> Выбрать:";

            for (var i = 0; i < rangesLength; i++)
            {
                html += "<span data-range='"+i+"'>"+this.options.ranges[i].name+"</span>";
            }

            html += "</div>";

            html += "<span class='lxrange-close'></span>";

            html += "</div>";

            return html;
        },

        _initDatepickers: function() {
            this.options.$lxrangestart.datepicker(this.options);
            this.options.$lxrangeend.datepicker(this.options);
        },

        _getCreateOptions: function() {
            var that = this;

            return {
                beforeShowDay: function ( date ) {
                    var blocker = true,
                        time = date.getTime(),
                        selectRange;

                    if ($(this).hasClass("lxrange-start") && that.options.end !== -1) {
                        blocker = (time <= that.options.end);
                    } else if ($(this).hasClass("lxrange-end") && that.options.start !== -1) {
                        blocker = (time >= that.options.start);
                    }

                    selectRange = (time >= that.options.start && time <= that.options.end && that.options.start !== -1) ? 'date-range-selected' : '';

                    return [(time <= that.options.currDate) && blocker, selectRange];
                },

                afterShow: function(){
                    that.options.$dialog.find(".ui-datepicker tr").find(".date-range-selected:first").addClass("first");
                    that.options.$dialog.find(".ui-datepicker tr").find(".date-range-selected:last").addClass("last");

                    if (that.options.$dialog.find(".ui-datepicker:first").find(".date-range-selected").length === 1 && that.options.$dialog.find(".ui-datepicker:last").find(".date-range-selected").length === 1) {

                        that.options.$dialog.find(".ui-datepicker tr").find(".date-range-selected").addClass("only");
                    }
                },

                onSelect: function ( dateText, inst ) {
                    that.options.currDate = new Date().getTime();

                    if ($(inst.input).hasClass("lxrange-start") || that.options.start === -1) {
                        that.options.start = (new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)).getTime();
                    }

                    if ($(inst.input).hasClass("lxrange-end") || that.options.end === -1) {
                        that.options.end = that.msTillDayEnd((new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)).getTime());
                    }

                    that.refreshDatepickers();
                    that.changeLabelText(dateText);
                    that.rangeChanged();

                },

                dateFormat: "dd.mm.yy",
                showOtherMonths: true

            };
        },

        checkToday: function(checkedStamp, mainStamp) {

            var sameDay = checkedStamp === -1 || (new Date(checkedStamp).toTimeString() === new Date(mainStamp).toTimeString());

            return sameDay;

        },

        msTillDayEnd: function(timestamp) {
            return timestamp + 23*60*60*1000 + 59*60*1000 + 59*1000;
        },

        _bindEvents: function() {
            this.options.$el.on("click", this, this.dialogToggle);
            this.options.$dialog.on("click", ".lxrange-close", this, this.closeDialog);

            $(document).on("click", this, this.handleGlobalClick);
            this.options.$dialog.on("click", ".lxrange-standard span", this, this.standardRangesClick);
        },

        handleGlobalClick: function(e) {
            var that = e.data;

            if (that.dialogIsShown()) {

                if (isEqual($(e.target), that.options.$dialog) || isEqual($(e.target), that.options.$label) || isEqual($(e.target), that.options.$label_text) || isEqual($(e.target), that.options.$el) || $(e.target).parents(".ui-datepicker-prev, .ui-datepicker-next ").length || isEqual($(e.target), that.options.$el) || $(e.target).parents("."+that.options.$dialog.prop("class")).length) {
                    return;
                }

                that.closeDialog(e);
            }
        },

        dialogIsShown: function() {
            return this.options.$dialog.is(":visible")
        },

        rangeChanged: function() {
            this.options.changed = true;
        },

        dialogToggle: function(e) {
            var that = e.data;

            if (that.dialogIsShown()) {
                return;
            }

            that.options.$dialog.show();
        },

        closeDialog: function(e) {
            var that = e.data;
            that.options.$dialog.hide();

            that.options.$label.trigger("lxrange-change");

            that.options.changed = false;
        },

        standardRangesClick: function(e) {
            var el = e.target;
            var that = e.data;
            var i = $(el).data("range");

            that.options.start = that.options.ranges[i].range(that.options.currDate);
            that.options.end = new Date().getTime();

            that.refreshDatepickers();

            $(".ui-state-active").removeClass("ui-state-active");

            that.changeLabelText();
            that.rangeChanged();
        },

        refreshDatepickers: function() {
            this.options.$lxrangestart.datepicker("refresh");
            this.options.$lxrangeend.datepicker("refresh");
        },

        changeLabelText: function() {
            var that = this;

            var startDate = new Date(that.options.start).toDateString(), endDate = new Date(that.options.end).toDateString();
            if (startDate === endDate || (that.options.start === -1 && that.options.end !== -1) || (that.options.end === -1 && that.options.start !== -1)) {
                that.setInputDate(that.options.$label_text, new Date(that.options.start));
            } else {
                that.setInputDate(that.options.$label_text, new Date(that.options.start), new Date(that.options.end));
                that.setInputDate(that.options.$inner_label, new Date(that.options.start), new Date(that.options.end));
            }

        },

        setInputDate: function(input, started, ended) {
            var d1, d2;
            d1 = $.datepicker.formatDate( this.options.dateFormat, started, {});

            if (ended !== undefined) {
                d2 = $.datepicker.formatDate( this.options.dateFormat, ended, {} );
                input.text( "С "+ d1 + " по " + d2 );
            } else {
                input.text(d1);
            }

        },

        getTimeStamp: function() {
            return {
                changed: this.options.changed,
                from: this.resetTimezone(this.options.start),
                to: this.resetTimezone(this.options.end)
            }
        },

        setTimeStamp: function(from, to) {
            this.options.start = this.resetTimezone(from, true);
            this.options.end = this.resetTimezone(to, true);

            this.refreshDatepickers();
            this.changeLabelText();
        },

        resetTimezone: function(ms, undo) {
            if (!this.options.resetTimezone) {return;}

            var date = new Date(ms);
            var offset = date.getTimezoneOffset()*60000;

            if (typeof undo === "boolean") {
                offset = undo ? offset * (-1) : offset;
            }

            return ms - offset;
        }

    };

    $.widget("ui.lxrangedatepicker", LXRangeDatepicker);

})(jQuery);